
#include "main.h"

SOCKADDR_IN SockAddr;
SOCKET Socket;
j3f_expecting expecting = J3F_CATEGORIES;
bool JK2 = false;

int main(int argc, char *argv[])
{
	if(argc == 2)
		if(strcmp(argv[1], "JK2") == 0)
			JK2 = true;

	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		printf("WSAStartup failed!\n");
		return FALSE;
	}

	struct hostent* pHE = gethostbyname("jediknight3.filefront.com");
	if(!pHE)
	{
		printf("jk3files is down. IT'S TOO LATE!!!\n");
		system("pause");
		return FALSE;
	}
	u_long addr = *((u_long*)pHE->h_addr_list[0]);
	if(!addr)
	{
		printf("gethostbyname failed!\n");
		return FALSE;
	}

	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = htons(80);
	SockAddr.sin_addr.s_addr = addr;

	Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(connect(Socket, (SOCKADDR*)(&SockAddr), sizeof(SockAddr)) == INVALID_SOCKET)
	{
		printf("connect failed!\n");
		return FALSE;
	}

	char urls[MAX_URLS][MAX_URL_LEN] = { 0 };
	int num_url = 0;
	int curr_url = 0;
	file_t *files= NULL;
	int num_file = 0;
	int curr_file = 0;

	int num_page = 0;

	int num_files = 0; long num_dl = 0; double num_GB = 0;
	while(1)
	{
		if(expecting == J3F_CATEGORIES)
		{
			J3F_GetData(
				JK2 ? "/files/Jedi_Knight_II;7index" : "/files/Jedi_Knight_III;38index", 
				urls, &num_url, 
				NULL, &num_file, 
				&num_page, NULL, 
				&num_files, &num_dl, &num_GB
				);

			if(!files)
			{
				if(JK2)
					num_files+=28; // I have absolutely no idea why I have to do this
				files = (file_t*)malloc(num_files*sizeof(file_t));
				memset(files, 0, num_files*sizeof(file_t));
			}

			if(num_page == 0)
				expecting = J3F_FILES;
		}
		else if(expecting == J3F_FILES)
		{
			/*FILE *file = fopen("files.txt", "r");
			if(file && num_files)
			{
				int n_files = 0;
				char line[MAX_FILE_LEN] = { 0 };
				while(fgets(line, sizeof(line), file))
					n_files++;
				fclose(file);
				if(n_files == num_files-1)
				{
					// Reload the file list if the number of files is the same
					file = fopen("files.txt", "r");
					while(fgets(line, sizeof(line), file))
					{
						strncpy(files[num_file].dir, line, strchr(line, ';')-line);
						strncpy(files[num_file].file, line+(strchr(line, ';')-line)+1, MAX_FILE_LEN);
						files[num_file].file[strlen(files[num_file].file)-1] = 0;
						num_file++;
					}
					fclose(file);
				}
			}*/

			if(num_file == num_files-1)
			{
				expecting = J3F_FILEPAGE;
			}
			else
			{
				J3F_GetData(urls[curr_url], 
					urls, &num_url, 
					files, &num_file, 
					&num_page, &curr_url, 
					NULL, NULL, NULL
					);
				system("cls");
				printf("Getting file list... %04i/%i\n", num_file, num_files-1);
			}
		}
		else if(expecting == J3F_FILEPAGE)
		{
			if(curr_file == num_files-1)
				break;
			char title[64] = { 0 };
			sprintf(title, "%04i/%i", curr_file, num_files-1);
			SetConsoleTitle(title);
			J3F_DownloadFile(files[curr_file++]);
		}
	}
	free(files);
	printf("Done!\n");
	system("pause");
	return TRUE;
}
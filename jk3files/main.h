
#include <direct.h> // for _getcwd
#include <assert.h> // for assert
#include "stdio.h"
#include "winsock2.h"
#include "windows.h"

// macros
#define MAX_URLS 128
#define MAX_URL_LEN 256

#define MAX_FILES 25
#define MAX_FILE_LEN 256

// variables, structs and enums
typedef enum j3f_expecting
{
	J3F_CATEGORIES, 
	J3F_FILES, 
	J3F_FILEPAGE, 
	J3F_FILE
};

typedef struct file_s
{
	char file[MAX_FILE_LEN];
	char dir[MAX_URL_LEN];
} file_t;

extern SOCKET Socket;
extern SOCKADDR_IN SockAddr;
extern j3f_expecting expecting;

// secondary functions
void J3F_GetData(
	const char *page, 
	char urls[][MAX_URL_LEN], int *num_url, 
	file_t *files, int *num_file, 
	int *num_page, int *curr, 
	int *num_files, long *num_dl, double *num_GB
	);

void J3F_DownloadFile(const file_t url);

/*
		---	The Scooper's guide to double arrays	---
			TODO: Get Scooper to write the guide
		---				The End						---
*/
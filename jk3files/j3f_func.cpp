
#include "main.h"

int recieve(char *rec, size_t max_size)
{
	size_t rec_size = 0;

	int n;
	char *f = rec;
	while(1)
	{
		n=recv(Socket, f, (max_size-rec_size)-1, 0);
		if(n<=0)
			break;
		rec_size+=n;
		f+=n;
	}
	return n;
}

int reconn(void)
{
	Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if(Socket == INVALID_SOCKET)
		return 0;

	if(connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr)) == INVALID_SOCKET)
	{
		closesocket(Socket);
		return 0;
	}

	return 1;
}
#define RECONN while(!reconn());

int change_host(const char *host)
{
	SockAddr.sin_addr.s_addr = *((u_long*)gethostbyname(host)->h_addr_list[0]);
	return SockAddr.sin_addr.s_addr ? 1 : 0;
}

void SomeWrapperThing(const char *url, const char *host, const char *cookie1, const char *cookie2, char **rec, size_t size)
{
	char cookies[512] = { 0 };
	if(*cookie1 && *cookie2)
		sprintf(cookies, "cookie: PHPSESSID=%s; SERVERID=%s; path=/\r\n", cookie1, cookie2);
	char buf[1024] = { 0 };
	sprintf(buf, 
		"GET %s HTTP/1.0\r\n"
		"host: %s\r\n"
		"user-agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)\r\n"
		"connection: close\r\n"
		"%s"
		"\r\n"
		, url, host, cookies);

	while(!**rec)
	{
		RECONN

		send(Socket, buf, strlen(buf), 0);

		if(recieve(*rec, size) == -1)
		{
			int err = GetLastError();
			printf(__FUNCTION__" %i\n", err);
			if(rec)
				memset(rec, 0, size);
		}
	}
}

void J3F_GetData(
	const char *page, 
	char urls[][MAX_URL_LEN], int *num_url, 
	file_t *files, int *num_file, 
	int *num_page, int *curr, 
	int *num_files, long *num_dl, double *num_GB
	)
{
	char url[1024] = { 0 };
	sprintf(url, 
		"%s"
		"?start=%i"
		"&sort=name"
		"&name_direction=asc"
		"&limit=%i"
		"&descriptions_in=0"
		"&summary_in=0"
		"&show_screenshot_in=0", page, *num_page*MAX_FILES, MAX_FILES);

	size_t size = 8*(1024*1024);
	char *rec = (char*)malloc(size);
	memset(rec, 0, size);
	SomeWrapperThing(url, "jediknight3.filefront.com", "", "", &rec, size);

	// Get categories
	char *tt;
	char *t = strstr(rec, "<!-- File Browser -->");
	t[strstr(t, "<!-- //File Browser -->")-t] = 0;

	while(t = strstr(t++, "class=\"size16\" href=\"/files/"))
	{
		char temp[MAX_URL_LEN] = { 0 };
		t+=strlen("class=\"size16\" href=\"");
		strncpy(temp, t, strstr(t, "\"")-t);
		if(strncmp(temp+strlen(temp)-strlen("index"), "index", strlen("index")) != 0)
		{
			// can't believe I'm doing this...
			bool exists = false;
			for(int i=0; i<*num_url; i++)
			{
				if(strcmp(urls[i], temp) == 0)
				{
					exists = true;
					break;
				}
			}
			if(!exists)
				strcpy(urls[(*num_url)++], temp);
		}
		t = strstr(t, "<br />"); assert(t); t+=strlen("<br />");
		int t1; int t2; double t3;
		if(num_files && num_dl && num_GB && sscanf(t, "%i Files - %i Downloads - %lf GB", &t1, &t2, &t3) == 3)
		{ *num_files+=t1; *num_dl+=t2; *num_GB+=t3; }
		tt = t;
	}

	// Get files
	t = tt;
	//FILE *file = fopen("files.txt", "a+");
	while(t = strstr(t, "<a class=\"size11\" href=\"/file/"))
	{
		char temp[MAX_FILE_LEN] = { 0 };
		t+=strlen("<a class=\"size11\" href=\"");
		strncpy(temp, t, strstr(t, "\"")-t);
		if(*num_file == 0 || strncmp(temp, files[(*num_file)-1].file, strlen(files[(*num_file)-1].file)) != 0)
		{
			printf("%s\n", temp);
			strncpy(files[*num_file].dir, page, strstr(page, ";")-page);
			strcpy(files[*num_file].file, temp);
			
			// Append the file to the list
			//fprintf(file, "%s;%s\n", files[*num_file].dir, files[*num_file].file);

			// Create directory if necessary
			char dir[1024] = { 0 };
			_getcwd(dir, sizeof(dir));
			char temp[1024] = { 0 };
			strcpy(temp, files[*num_file].dir);
			char *p = strtok(temp, "/");
			while(p)
			{
				dir[strlen(dir)] = '\\';
				strncat(dir, p, sizeof(dir));
				CreateDirectory(dir, NULL);
				p = strtok(p+strlen(p)+1, "/");
			}
			(*num_file)++;
		}
		tt = t;
	}
	//fclose(file);

	if(strstr(tt, "Next "))
		(*num_page)++;
	else
	{
		*num_page = 0;
		if(curr) (*curr)++;
	}

	free(rec);
}

// This function could've been split in like 6 seperate ones, but I was like nah
void J3F_DownloadFile(const file_t/*char **/file)
{
	// Get the link to GameFront first...

	// Set the default server
	change_host("jediknight3.filefront.com");

	size_t size = 10*(1024*1024); // 10mb/s should be a good limit
	char *rec = (char*)malloc(size); *rec = 0;
	memset(rec, 0, size);
	SomeWrapperThing(file.file, "jediknight3.filefront.com", "", "", &rec, size);

	// BREAK MEDIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	while(strstr(rec, "Invalid File ID!"))
	{
		memset(rec, 0, size);
		SomeWrapperThing(file.file, "jediknight3.filefront.com", "", "", &rec, size);
	}

	// HERE, IT'S OVER HERE
	char *t;
	char cookie1[1024] = { 0 };
	char cookie2[1024] = { 0 };

	t = strstr(rec, "Filename: <i>")+strlen("Filename: <i>"); assert(t);
	char t_filename[1024] = { 0 };
	strncpy(t_filename, t, strstr(t, "<")-t);

	FILE *fi;
	char filename[1024] = { 0 };
	sprintf(filename, "%s/%s", file.dir, t_filename);
	// Check if the file already exists
	fi = fopen(filename+1, "r");
	if(fi)
	{
		fclose(fi);
		free(rec);
		return;
	}

	if(strstr(rec, "<b>Download Here:</b> <a href=\"http://www.gamefront.com/files/"))
	{
		t = strstr(rec, "<b>Download Here:</b> <a href=\"http://www.gamefront.com/files/")+strlen("<b>Download Here:</b> <a href=\"http://www.gamefront.com");
		char linkr[1024] = { 0 };
		strncpy(linkr, t, strstr(t, "\"")-t);
		memset(rec, 0, size);
		change_host("gamefront.com");
		SomeWrapperThing(linkr, "www.gamefront.com", "", "", &rec, size);
		goto redir;
	}
	t = strstr(rec, "<a class=\"size16\" href=\"")+strlen("<a class=\"size16\" href=\"");
	char link1[1024] = { 0 };
	strncpy(link1, t, strstr(t, "\">")-t);

	system("cls");
	printf("Downloading %s/%s... Stage 1/6 done!\n", file.dir, t_filename);

	// Then the link to the actual file FROM GameFront...
	memset(rec, 0, size);
	SomeWrapperThing(link1, "jediknight3.filefront.com", "", "", &rec, size);

	t = strstr(rec, "Location: http://www.filefront.com/?file")+strlen("Location: http://www.filefront.com/?file"); assert(t);
	char link2[1024] = { 0 };
	strncpy(link2, t, strstr(t, "\r")-t);

	system("cls");
	printf("Downloading %s/%s... Stage 2/6 done!\n", file.dir, t_filename);

	// Now since this was a redirect (302 Found), we need to get another page...

	// We also need to switch servers now
	change_host("gamefront.com");

	memset(rec, 0, size);
	char url_temp[1024] = { 0 };
	sprintf(url_temp, "/files/files/service/external?%s", link2);
	SomeWrapperThing(url_temp, "www.gamefront.com", "", "", &rec, size);

	t = strstr(rec, "Location: ")+strlen("Location: "); assert(t);
	char link3[1024] = { 0 };
	strncpy(link3, t, strstr(t, "\r")-t);

	if(strcmp(link3, "/files/files/index/noroute") == 0)
	{
		free(rec);
		return;
	}
	
	// GET THE COOKIES HERE
	t = strstr(rec, "Set-Cookie: PHPSESSID=")+strlen("Set-Cookie: PHPSESSID="); assert(t);
	strncpy(cookie1, t, strstr(t, ";")-t);
	t = strstr(rec, "Set-Cookie: SERVERID=")+strlen("Set-Cookie: SERVERID="); assert(t);
	strncpy(cookie2, t, strstr(t, ";")-t);

	system("cls");
	printf("Downloading %s/%s... Stage 3/6 done!\n", file.dir, t_filename);

	// Aaaand another redirect...
	memset(rec, 0, size);
	SomeWrapperThing(link3, "www.gamefront.com", cookie1, cookie2, &rec, size);

redir:
	t = strstr(rec, "<a href=\"http://www.gamefront.com/files/service/thankyou?id=")+strlen("<a href=\"http://www.gamefront.com"); assert(t);
	char link4[1024] = { 0 };
	strncpy(link4, t, strstr(t, "\"")-t);
	
	system("cls");
	printf("Downloading %s/%s... Stage 4/6 done!\n", file.dir, t_filename);

	// Yep, another one...
	memset(rec, 0, size);
	SomeWrapperThing(link4, "www.gamefront.com", cookie1, cookie2, &rec, size);

	// FUUUUUCK YOU, BREAK MEDIA!!!
	if(strncmp(rec, "HTTP/1.1 301 Moved Permanently", strlen("HTTP/1.1 301 Moved Permanently")) == 0)
	{
		// AND ALSO HERE, WE ARE GETTING COOKIES HERE TOO
		t = strstr(rec, "Set-Cookie: PHPSESSID=")+strlen("Set-Cookie: PHPSESSID="); assert(t);
		strncpy(cookie1, t, strstr(t, ";")-t);
		t = strstr(rec, "Set-Cookie: SERVERID=")+strlen("Set-Cookie: SERVERID="); assert(t);
		strncpy(cookie2, t, strstr(t, ";")-t);
		memset(rec, 0, size);
		SomeWrapperThing(link4, "www.gamefront.com", cookie1, cookie2, &rec, size);
		t = strstr(rec, "Location: ")+strlen("Location: "); assert(t);
		memset(&link4, 0, sizeof(link4));
		strncpy(link4, t, strstr(t, "\r")-t);
		memset(rec, 0, size);
		SomeWrapperThing(link4, "www.gamefront.com", cookie1, cookie2, &rec, size);
		goto redir;
	}

	t = strstr(rec, "<p>Your download will begin in a few seconds.<br />If it does not, <a href=\"")+strlen("<p>Your download will begin in a few seconds.<br />If it does not, <a href=\""); assert(t);
	char link5[1024] = { 0 };
	strncpy(link5, t, strstr(t, "\"")-t);
	
	/* Deprecated
	char t_filename[1024] = { 0 };
	t = strrchr(link5, '/')+1;
	strncpy(t_filename, t, strchr(t, '?')-t);
	*/
	system("cls");
	printf("Downloading %s/%s... Stage 5/6 done!\n", file.dir, t_filename);

	// Actually download the file

	// Switch server again
	change_host("media1.gamefront.com");

	memset(rec, 0, size);
	// We need some ADVANCED RECIEVING IN HERE
	char buf[1024] = { 0 };
	sprintf(buf, 
		"GET %s HTTP/1.0\r\n"
		"host: media1.gamefront.com\r\n"
		"user-agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)\r\n"
		"connection: keep-alive\r\n"
		"\r\n"
		, link5);
	RECONN
	send(Socket, buf, strlen(buf), 0);
	
	size_t rec_size = 0;
	int n;
	char *f = rec;
	bool gotHeaders = false;
	bool gotFullSize = false;
	size_t full_size;
	size_t sub_size;
	fi = fopen(filename+1, "ab+");
	while(1)
	{
		n=recv(Socket, rec, size-1, 0);
		if(n<=0)
			break;
		rec_size+=n;
		//f+=n;
		if(!gotHeaders && strstr(rec, "\r\n\r\n"))
		{
			f=strstr(rec, "\r\n\r\n")+strlen("\r\n\r\n");
			sub_size = f-rec;
			gotHeaders = true;
		} else f = rec;
		fwrite(f, 1, n, fi);
		if(!gotFullSize && strstr(rec, "Content-Length: ") && strstr(rec, "\r\n\r\n"))
		{
			char n_size[64] = { 0 };
			t = strstr(rec, "Content-Length: ")+strlen("Content-Length: "); assert(t);
			strncpy(n_size, t, strchr(t, '\r')-t);
			full_size = atoi(n_size);
			gotFullSize = true;
		}
		if(gotFullSize)
		{
			system("cls");
			printf("Downloading %s/%s... Stage 5/6 done!\n%i/%i\n", file.dir, t_filename, rec_size-sub_size, full_size);
			if(rec_size-sub_size == full_size)
				break; // hopefully this MIGHT fix that hanging issue Syko has been having
		}
	}
	if(n==-1)
	{
		int err = GetLastError();
		printf(__FUNCTION__" %i\n", err);
	}
	fclose(fi);
	free(rec);
	system("cls");
	printf("Downloading %s/%s... Stage 6/6 done!\n", file.dir, t_filename);
	return;
}